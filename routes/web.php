<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('welcome');
    });

    //to test push notification
    Route::get('test', function () {
        event(new App\Events\StatusLiked(Auth::user()->name));
        return "Event has been sent!";
    });
});
Auth::routes();
