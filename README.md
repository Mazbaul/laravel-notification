#laravel push notification

#Get the source code on your machine via git
 - clone project
 - Rename file .env.example to .env and change credentials.
 - Create a database and inform .env

 #go to project folder
- cd projectname
- composer install
- php artisan key:generate
- php artisan migrate --seed to create and      populate tables
- php -S localhost:8000 -t public to start the app on http://localhost:8000/

 -register new user
 -login to the system
 -to test push notification http://localhost:8000/test browse this link 
